#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <math.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    centralWidget()->layout()->setContentsMargins(0,0,0,0);
    centralWidget()->layout()->setSpacing(0);

    connect(ui->actionRun, &QAction::triggered, this, &MainWindow::sendCommand);
    connect(&serial, &QSerialPort::readyRead, this, &MainWindow::readData);
    connect(&serial, SIGNAL(errorOccurred(QSerialPort::SerialPortError)), this, SLOT(error(QSerialPort::SerialPortError)));


    ui->plot->setSeries(1, DSIZE2);
    ui->plot->plotMode=Plot::LinearPlot;
    ui->plot->plotColor[0]=Qt::red;
    ui->plot->setRange(0, DSIZE2-1, -1, 1);
    ui->plot->setAxes(10, 0,  1000*DSIZE2/FS, 10, -1, 1);


    ui->plot_2->setSeries(2, DSIZE2);
    ui->plot_2->plotMode=Plot::LinearPlot;
    ui->plot_2->plotColor[0]=Qt::green;
    ui->plot_2->plotColor[1]=Qt::yellow;
    ui->plot_2->setRange(0, DSIZE2-1, 0, 1);
    ui->plot_2->setAxes(10, 0,  1000*DSIZE2/FS, 10, 0, 1);

    maska.resize(MSIZE);
    maska.fill(1.0/MSIZE);

    ui->statusbar->showMessage("No device");
    QString portname;
    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos) {
        if (info.description()=="Urządzenie szeregowe USB") {
            portname=info.portName();
            serial.setPortName(portname);
            if (serial.open(QIODevice::ReadWrite)) {
                ui->statusbar->showMessage(tr("Device: %1").arg(info.description()));
                serial.clear();
                ui->statusbar->setEnabled(true);
            } else {
                ui->statusbar->showMessage(tr("Can't open %1, error code %2") .arg(serial.portName()).arg(serial.error()));
                return;
            }
            break;
        }
    }
}

MainWindow::~MainWindow()
{
    serial.close();
    delete ui;
}

void MainWindow::sendCommand()
{
    senddata.clear();
    senddata.resize(1);
    senddata[0]=static_cast<uint8_t>(ui->actionRun->isChecked() << 7); // Run/Stop
    serial.write(senddata);
}



void MainWindow::readData()
{
    static uint32_t fr=0, fr2=0, counter = MSIZE/2;

    if (serial.size() >= RSIZE) {

        readdata=serial.read(RSIZE);
        qDebug()<<readdata.size();

        uint16_t *sample=reinterpret_cast<uint16_t*>(readdata.data());

        // remove DC offset
        double dc=0;
        for (int n=0; n<RSIZE2; n++) {
            dc+=(sample[n]-32768)/32768.0;
        }
        dc/=RSIZE2;

        for (int n=0; n<RSIZE2; n++) {
            // Raw data - DC offset
            ui->plot->dataSeries[0][fr+n]= ((sample[n]-32768)/32768.0)-dc;
            ui->plot_2->dataSeries[0][fr+n]=abs(ui->plot->dataSeries[0][fr+n]);
        }


        if (fr==0){
            for (int j=0; j<RSIZE2-MSIZE; j++) {
                for (int i=0;i<MSIZE; i++){
                    avg+=ui->plot_2->dataSeries[0][fr2+j+i]*maska[i];
                }
                ui->plot_2->dataSeries[1][counter]=avg;
                avg=0.0;
                counter++;
                if(counter>=DSIZE2-(MSIZE/2))
                    counter=MSIZE/2;
            }
            fr2+=RSIZE2-MSIZE;
        }else{
            for (int j=0; j<RSIZE2; j++) {

                for (int i=0;i<MSIZE; i++){
                    avg+=ui->plot_2->dataSeries[0][fr2+j+i]*maska[i];
                }
                ui->plot_2->dataSeries[1][counter]=avg;
                avg=0.0;
                counter++;
                if(counter>=DSIZE2-(MSIZE/2))
                    counter=MSIZE/2;
            }
            fr2+=RSIZE2;
        }
        if (fr2>=DSIZE2-MSIZE)
            fr2=0;
    }



    fr+=RSIZE2;
    if(fr>=DSIZE2)
        fr=0;

    ui->plot->update();
    ui->plot_2->update();
}


void MainWindow::error(QSerialPort::SerialPortError error)
{
    qDebug()<<error;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    Q_UNUSED(event);
    if(ui->actionRun->isChecked())
        ui->actionRun->trigger();
}

void MainWindow::calculateFFT()
{
    fftData.fill(0);
    for(int i=0;i<DSIZE2; i++)
        fftData[static_cast<uint>(i)].real(ui->plot->dataSeries[0][i]*fftWin[i]);

    fftData=arma::fft(fftData);

    for(int i=0;i<FFT_SIZE2; i++) {
        magnitudeData[i]=abs(fftData[static_cast<uint>(i)])/(FFT_SIZE2);
        phaseData[i]=arg(fftData[static_cast<uint>(i)]);
    }
}


void MainWindow::on_actionSave_triggered()
{
    QFile file("DaneEMG.txt");
    if(file.open(QIODevice::WriteOnly | QIODevice::Text)){
        QTextStream out(&file);
        for (QVector<double>::iterator iter = ui->plot->dataSeries[0].begin(); iter != ui->plot->dataSeries[0].end(); iter++){
            out << *iter<<";";
        }
        file.close();
    }

}

void MainWindow::on_actionLoad_triggered()
{
    //    QFile file("DaneEMG.txt");
    //        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    //            return;

    //        QTextStream in(&file);
    //        while (!in.atEnd()) {
    //            for (int n=0; n<RSIZE2; n++) {
    //                ui->plot->dataSeries[0][n] << in.readLine().toDouble();
    //             }
    //        }
}
